[![Docker Pulls](https://img.shields.io/docker/pulls/dtulyakov/curl.svg)][hub]

[hub]: https://hub.docker.com/r/dtulyakov/curl/

```BASH
docker run --rm dtulyakov/curl -fsSL https://ya.ru/
```
